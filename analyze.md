用go实现一个简单的基于tcp的缓存服务。
要求：
1.满足存储键值对的需求，存储的值为字符串( []rune <=> string <=> []byte )；
2.需要实现create/update/delete三个操作( create|update<=>set,delete<=>del)；
3.需要实现缓存过期自动清除( time,应该结合go程，不然就放那直接访问时再判断过期时间进行伪自动删除了 )。

Ayalyze:( 存于内存，关了就gg )
方案一：
--server：服务端
----tcp
------tcp链接的创建、链接==new.go
------增删改 数据处理（间接用到cache的方法）==process.go
----cache：真正拿来搞缓存( map[string][]byte )
------增删改接口（cache接口，为了通用，interface，GET|DEL|SET）==cache.go
------创建 cache ==new.go
------实现 cache 接口==inmemory.go
------缓存内容的查询（额外，估计砍掉）==stat.go
----启动类==main.go

server结构体
--clients存client
--command存合法命令cmd
--map当缓存
--

--client：客户端
----启动类==main.go(一直能够输入)
