package main

import (
	"fmt"
	"goTcpCache/server/cache"
	"log"
	"net"
	"os"
	"os/signal"
	"strconv"
	"sync"
	"syscall"
	"time"
)

var server = new(cache.Server)
var serverStopChannel = make(chan struct{}, 1)
var wg sync.WaitGroup

func main() {
	// 1.信号监听处理
	c := make(chan os.Signal, 1)
	// interrupt,terminated,quit
	signal.Notify(c, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT, syscall.SIGUSR1, syscall.SIGUSR2, syscall.SIGHUP)
	go signalHandler(c)

	// 2.初始化服务器实例
	initServer()

	// 3.链接处理
	tcpAddr, err := net.ResolveTCPAddr("tcp", "127.0.0.1:9999")
	if err != nil {
		log.Println("ResolveTCPAddr err:", err)
		serverStopChannel <- struct{}{}
		wg.Wait()
		return
	}
	tcpListener, err := net.ListenTCP("tcp", tcpAddr)
	// Listener, err := net.Listen("tcp", "127.0.0.1:9999")
	if err != nil { // 链接建立失败，把之前初始化时创建的server go程取消
		log.Println("ListenTCP err:", err)
		serverStopChannel <- struct{}{}
		wg.Wait()
		return
	}
	log.Println("==>>My server is ready go!!!")
	defer tcpListener.Close()
	for {
		tcpConn, err := tcpListener.AcceptTCP()
		if err != nil {
			// 出问题就byebye了
			log.Println("tcpListener.AcceptTCP() Error:", err)
			continue
		}
		go handle(tcpConn)
	}
}

// 初始化服务器实例参数
func initServer() {
	// 初始化DB
	server.DbNum = 3
	initDb()
	// 初始化server端命令集
	getCommand := &cache.RocCommand{Name: "get", Proc: cache.GetCommand}
	SetCommand := &cache.RocCommand{Name: "set", Proc: cache.SetCommand}
	DelCommand := &cache.RocCommand{Name: "del", Proc: cache.DelCommand}
	TtlCommand := &cache.RocCommand{Name: "ttl", Proc: cache.TtlCommand}
	ExpireCommand := &cache.RocCommand{Name: "expire", Proc: cache.ExpireCommand}
	KeysCommand := &cache.RocCommand{Name: "keys", Proc: cache.KeysCommand}
	SelectCommand := &cache.RocCommand{Name: "select", Proc: cache.SelectCommand}
	server.Commands = map[string]*cache.RocCommand{
		"get":    getCommand,
		"set":    SetCommand,
		"del":    DelCommand,
		"ttl":    TtlCommand,
		"expire": ExpireCommand,
		"keys":   KeysCommand,
		"select": SelectCommand,
	}
	// 检查各DB中的缓存是否过期
	wg.Add(1)
	go func(s *cache.Server, stop chan struct{}) {
		// 打印分割行的
		var printFlag = false
		for {
			select {
			case <-stop: // 当server结束的时候顺便把伴生的routine送走
				log.Println("==>>serverCheckExpireGoRoutine Over!!!")
				// s.NotifyServerClosed(cache.NotifyCh)
				wg.Done()
				// 不通知的话客户端会连着已经关闭的s
				// 那在这里发送s已经停了的通知给客户端然后直接断掉客户端的连接？
				return
			default:
				for i, db := range s.Db { // []*RocDb
					for keyStr, valObj := range db.Expires { // map[string]time.Time
						log.Println("key: "+keyStr+", val:", valObj.Ptr.(time.Time))
						fmt.Println("test: ", int64(time.Since(valObj.Ptr.(time.Time))/time.Second))
						if time.Since(valObj.Ptr.(time.Time))/time.Second >= 0 {
							delete(s.Db[i].Dict, keyStr)
							delete(s.Db[i].Expires, keyStr)
						}
						printFlag = true
					}
					if printFlag {
						log.Println("========s.Db[" + strconv.Itoa(i) + "]========")
						printFlag = false
					}
				}
			}
			time.Sleep(3 * time.Second)
		}
	}(server, serverStopChannel)
	// 初始化服务器参数完成
	log.Println("==>>init server success")
}

// 初始化Db
func initDb() {
	server.Db = make([]*cache.RocDb, server.DbNum)
	for i := 0; i < server.DbNum; i++ {
		server.Db[i] = new(cache.RocDb)
		server.Db[i].Dict = make(map[string]*cache.SpecObject, 88)
		server.Db[i].Expires = make(map[string]*cache.SpecObject, 88)
	}
	log.Println("==>>init server'Db success")
}

// 处理请求（v0.0.2）
func handle(tcpConn *net.TCPConn) {
	// 1.创建client（PS：不是所谓的客户端）
	c := server.CreateClient(tcpConn)
	// 直接死循环，不行，卡住后边==>>goroutine polling轮询
	// stop := make(chan struct{}, 1)
	// go func(c *cache.Client, stop chan struct{}) {
	// 	var printFlag = false
	// 	for {
	// 		select {
	// 		case <-stop: // 客户端gg了，我也走了
	// 			log.Println("┭┮﹏┭┮, byebye, stop routine")
	// 			return
	// 		default: //查查谁过期了
	// 			// 有个小问题，如果c和s都运行着，啥事没有
	// 			// 1.如果s运行着，c退出了，键值对也都过期了，啥事没有
	// 			// 2.如果s运行着，c退出了，键值对还没过期，此时有c进来，然后键值对还是没过期，啥事没有
	// 			// 3.如果s运行着，c退出了，键值对还没过期，但新的c进来的时候键值对已经或者恰好过期了，
	// 			//   因为把手动删除的代码注释了，所以get的时候虽然是返回nil，但是其实还没删除，可以自己手动删或者等到再次轮询到自动删
	// 			//   （此种情况在有新的c连进来之后可以自动删除掉过期的键值对，但要是一直没c连进来，那么过期的就一直在，这是个问题点）
	// 			// 想法：
	// 			// 1、每个client进来后遍历自己的那DB去判断DB内缓存是否过期从而选择删除
	// 			// 2、另一种是server单独自己开一个go程，然后反正server不关，就自己去遍历所有DB里的缓存去判断是否删除，目前只有一个DB，遍历c或者s都一样，运行时长问题而已
	// 			// 但对那种很多DB都有数据的这策略可能不大行？但如果多个DB，对每个c都开一个goroutine跟只给s开一个goroutine去查，开销还是不同的，c太多也只是重复工作，s却不会
	// 			for keyStr, valObj := range c.Db.Expires {
	// 				log.Println("key: "+keyStr+", val:", valObj.Ptr.(time.Time))
	// 				fmt.Println("test: ", int64(time.Since(valObj.Ptr.(time.Time))/time.Second))
	// 				if time.Since(valObj.Ptr.(time.Time))/time.Second >= 0 {
	// 					delete(c.Db.Dict, keyStr)
	// 					delete(c.Db.Expires, keyStr)
	// 				}
	// 				printFlag = true
	// 			}
	// 			if printFlag {
	// 				log.Println("========split========")
	// 				printFlag = false
	// 			}
	// 		}
	// 		time.Sleep(3 * time.Second)
	// 	}
	// }(c, stop)

	log.Println("createClient for tcpConn: " + tcpConn.RemoteAddr().String())
	for {
		// 2.读取客户端请求信息
		err := c.ReadQueryInfoFromClient(tcpConn)
		if err != nil {
			log.Println("readQueryInfoFromClient err:", err)
			// stop <- struct{}{}
			return
		}
		// 3.处理客户端请求信息
		c.ProcessQueryBuf()
		// 4.执行命令
		server.ProcessCommand(c)
		// 5.响应给客户端
		responseToConn(tcpConn, c)
	}
}

// 响应返回给客户端
func responseToConn(tcpConn *net.TCPConn, c *cache.Client) {
	tcpConn.Write([]byte(c.ReplyBuf))
}

// 信号处理
func signalHandler(c chan os.Signal) {
	// 获取信号
	for s := range c {
		switch s {
		case syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT:
			exitHandler()
		default:
			fmt.Println("Signal:", s)
		}
	}
}

// 退出处理
func exitHandler() {
	serverStopChannel <- struct{}{}
	wg.Wait()
	fmt.Println("byebye")
	os.Exit(0)
}

// 处理连接请求(v0.0.1)
// func handle(conn net.Conn) {
// 	// 初处理
// 	for {
// 		// 读取客户端请求信息
// 		buff, err := readMsgFromClient(conn)
// 		if err != nil {
// 			log.Println("readMsgFromClient err")
// 			return
// 		}
// 		// 处理请求信息
// 		result := processInMsg(conn, buff)
// 		// 返回响应给客户端
// 		writeToClient(conn, result)
// 	}
// }

// func readMsgFromClient(conn net.Conn) (buf string, err error) {
// 	buff := make([]byte, 1024)
// 	n, err := conn.Read(buff)
// 	if err != nil {
// 		log.Print("conn.Read err: ", err, "--len--", n)
// 		conn.Close()
// 		return "", err
// 	}
// 	buf = string(buff)
// 	return buf, nil
// }

// func processInMsg(conn net.Conn, buff string) string {
// 	fmt.Println(conn.RemoteAddr().String()+">", buff)
// 	return "SUCCEED"
// }

// func writeToClient(conn net.Conn, buff string) {
// 	conn.Write([]byte(buff))
// }
