package cache

import (
	"fmt"
	"log"
	"regexp"
	"strconv"
	"strings"
	"time"
)

// 4.2.1.SetCommand set指令(create|update)
func SetCommand(c *Client, s *Server) {
	c.Db.mutex.Lock()
	defer c.Db.mutex.Unlock()
	// 拿要存储的键值
	keyObj := c.Argv[1]
	valObj := c.Argv[2]
	if c.Argc != 3 { // 键值不全
		addReplyBuf(c, CreateObject(ObjTypeString, "Error: missing arguments for 'set' command"))
		return
	}
	if keyStr, ok1 := keyObj.Ptr.(string); ok1 {
		// log.Println("keyStr:", keyStr)
		if valStr, ok2 := valObj.Ptr.(string); ok2 {
			// 查看是否已存在了对应的key
			// obj := findKey(c.Db, keyStr)
			// 需要存放的key不在，新添置，设置初始过期时间
			// 需要存放的key在家，换配置，重新设置过期时间
			c.Db.Dict[keyStr] = CreateObject(ObjTypeString, valStr)
			c.Db.Expires[keyStr] = CreateObject(ObjTypeString, time.Now().Add(GlobalExpireTime*time.Second))
			// c.Db.Expires[keyStr] = CreateObject(ObjTypeString, time.Now())
			// log.Println("setCommand:" + keyStr)
			addReplyBuf(c, CreateObject(ObjTypeString, "OK"))
			// addReplyBuf(c, CreateObject(ObjTypeString, fmt.Sprintf("set key '%s' OK", c.Db.Dict[keyObj.Ptr.(string)].Ptr.(string))))
		}
	} else {
		addReplyBuf(c, CreateObject(ObjTypeString, "Error: set failed"))
	}
	// log.Println("setCommand over:", c.Db.Dict[keyObj.Ptr.(string)])
}

// 4.2.2.GetCommand get指令
func GetCommand(c *Client, s *Server) {
	c.Db.mutex.RLock()
	defer c.Db.mutex.RUnlock()
	// 先查key？先查expired？map,随便啦
	obj := findKey(c.Db, c.Argv[1].Ptr.(string))
	expired := expired(c.Db, c.Argv[1].Ptr.(string))
	// obj := findKey(c.Db, c.Argv[1])
	// log.Println("arg:", c.Argv[1].Ptr.(string))
	if obj != nil && !expired {
		addReplyBuf(c, CreateObject(ObjTypeString, "\""+obj.Ptr.(string)+"\""))
	} else {
		addReplyBuf(c, CreateObject(ObjTypeString, "<nil>"))
		// if expired {
		// 	delete(c.Db.Dict, c.Argv[1].Ptr.(string))
		// 	delete(c.Db.Expires, c.Argv[1].Ptr.(string))
		// }
	}
}

// 4.2.3.DelCommand del指令(delete)
func DelCommand(c *Client, s *Server) {
	c.Db.mutex.Lock()
	defer c.Db.mutex.Unlock()
	obj := findKey(c.Db, c.Argv[1].Ptr.(string))
	if obj != nil {
		delete(c.Db.Dict, c.Argv[1].Ptr.(string))
		delete(c.Db.Expires, c.Argv[1].Ptr.(string))
		addReplyBuf(c, CreateObject(ObjTypeString, "(del success) 1"))
	} else {
		addReplyBuf(c, CreateObject(ObjTypeString, "(no such key) 0"))
	}
}

// 4.2.4.ExpireCommand expire指令(有效期时间)
func ExpireCommand(c *Client, s *Server) {
	c.Db.mutex.Lock()
	defer c.Db.mutex.Unlock()
	if c.Argc != 3 { // 键值不全
		addReplyBuf(c, CreateObject(ObjTypeString, "Error: missing arguments for 'expire' command(expected 2)"))
		return
	}
	// TODO 应该限制输入的秒数的格式，后期再找正则表达式加进去
	re := regexp.MustCompile(`^(0|[1-9][0-9]*)$`)
	// re := regexp.MustCompile(`^\+?[1-9][0-9]*$`)×××
	// re := regexp.MustCompile(`^[1-9][0-9]*|0$`)×××
	// fmt.Println("test:", re)
	keyObj := c.Argv[1]
	secObj := c.Argv[2]
	validNum := re.MatchString(secObj.Ptr.(string))

	// fmt.Println("validNum:", re.MatchString(secObj.Ptr.(string)))
	if validNum { // 合法输入
		obj := findKey(c.Db, keyObj.Ptr.(string))
		if obj != nil {
			// 自定义存活时间
			secVal, _ := strconv.Atoi(secObj.Ptr.(string)) // 新的存活秒数，应该从当前时间加上这段时间，存入Expires对应位置
			c.Db.Expires[keyObj.Ptr.(string)] = CreateObject(ObjTypeString, time.Now().Add(time.Duration(secVal*int(time.Second))))
			addReplyBuf(c, CreateObject(ObjTypeString, "(integer) 1"))
		} else {
			addReplyBuf(c, CreateObject(ObjTypeString, "(integer) 0"))
		}
	} else {
		addReplyBuf(c, CreateObject(ObjTypeString, "(error) ERR value is not a positive integer or zero"))
	}
}

// 4.2.5.TtlCommand ttl指令(查询存活时间time to live)
func TtlCommand(c *Client, s *Server) {
	c.Db.mutex.RLock()
	defer c.Db.mutex.RUnlock()
	obj := findKey(c.Db, c.Argv[1].Ptr.(string))
	if obj != nil {
		// 剩余存活时间=设定的过期时间[PS：还没有自定义的，只有全局] - (当前时间 - set键值对时加入Expires的时间)
		// 自定义也没必要，reids里设置expire后再次set也是重新设置为不过期了
		// ttl := GlobalExpireTime - (time.Since(c.Db.Expires[c.Argv[1].Ptr.(string)].Ptr.(time.Time)) / time.Second)

		// 上边推翻，下边重开
		// ttl := c.Db.Expires[c.Argv[1].Ptr.(string)].Ptr.(time.Time).Sub(time.Now())
		ttl := time.Until(c.Db.Expires[c.Argv[1].Ptr.(string)].Ptr.(time.Time))
		addReplyBuf(c, CreateObject(ObjTypeString, fmt.Sprint(int64(ttl/time.Second))))
	} else {
		addReplyBuf(c, CreateObject(ObjTypeString, "(Integer) -2 no such key"))
	}
}

// 4.2.6.KeysCommand keys指令（查询已存入的key）
func KeysCommand(c *Client, s *Server) {
	// 要遍历，就要遍历s，但有多个db，每个c都对应s中的某个db，那么查keys就应该只查c对应s中的db的keys
	// 这样子的话，就需要c有某个识别标志才行，可以加个ID，但这个ID用什么呢？s的DB下标？
	// 好像也不用，每个c创建的时候已经指向了s中的某个db，那么直接遍历c中的db，就跟try redis类似
	// 大体ok，如果要匹配某个key呢，正则可以解决，但就得判断keys的参数是 * 还是 某个key所含有的字符了
	// 三种: keys * | keys key* | keys *key
	// 直接把参数放进来当正则表达式就行了?
	c.Db.mutex.RLock()
	defer c.Db.mutex.RUnlock()
	var dictLen = len(c.Db.Dict)
	if dictLen <= 0 {
		addReplyBuf(c, CreateObject(ObjTypeString, "(empty)"))
	} else {
		// 查找全部分配全部，查找部分通过匹配来分配？
		// 全都按通过匹配来分配？
		if c.Argc != 2 {
			addReplyBuf(c, CreateObject(ObjTypeString, "(wrong argument number for keys command, expected 1)"))
		} else if c.Argc == 2 {
			var i int = 0
			var sb strings.Builder // 拼接匹配到的key
			var needToCompileRe string
			if strings.Contains(c.Argv[1].Ptr.(string), "*") { // 有带 * 就处理一下，没有就直接放
				needToCompileRe = strings.ReplaceAll(c.Argv[1].Ptr.(string), `*`, `\w*`)
			} else {
				needToCompileRe = c.Argv[1].Ptr.(string)
			}
			re := regexp.MustCompile("^" + needToCompileRe + "$")
			for keyStr := range c.Db.Dict {
				if re.Match([]byte(keyStr)) {
					i++
					sb.WriteString(strconv.Itoa(i) + ") \"" + keyStr + "\"\n") // 每个都换个行，最后再把末尾的换行处理掉吧
				}
			}
			if i == 0 { // 啥也没匹配到
				addReplyBuf(c, CreateObject(ObjTypeString, "(empty)"))
			} else {
				res := strings.TrimSuffix(sb.String(), "\n")
				addReplyBuf(c, CreateObject(ObjTypeString, res))
			}
		}
		// keysStrSlice := make([]string, 0, dictLen)
		// var i int = 0
		// for keyStr := range c.Db.Dict {
		// 	i++
		// 	if i == dictLen {
		// 		keysStrSlice = append(keysStrSlice, strconv.Itoa(i)+") \""+keyStr+"\"")
		// 	} else {
		// 		keysStrSlice = append(keysStrSlice, strconv.Itoa(i)+") \""+keyStr+"\"\n")
		// 	}
		// }
		// res := strings.Join(keysStrSlice, "")
		// addReplyBuf(c, CreateObject(ObjTypeString, res))
	}
}

// 4.2.7.SelectCommand select指令（切换指向s的DB）
func SelectCommand(c *Client, s *Server) {
	// 问题是如何标识出用的哪个DB，客户端那固定的没显示，只有IP Port
	// 客户端识别select然后把DB索引切到固定显示去进行更改？
	// addReplyBuf(c, CreateObject(ObjTypeString, "okok"))//OK
	// 1.判断参数个数是否齐全，一个select加一个下标参数够了
	if c.Argc != 2 {
		addReplyBuf(c, CreateObject(ObjTypeString, "(wrong argument number for select command, expected 1)"))
		return
	} else {
		// 2.判断切换的DB下标是否超出
		dbIx, err := strconv.Atoi(c.Argv[1].Ptr.(string))
		if err != nil {
			log.Println("(Error)", err.Error())
			return
		}
		if dbIx > (s.DbNum - 1) {
			// 2.1.超出，发出警告
			addReplyBuf(c, CreateObject(ObjTypeString, fmt.Sprintf("(Warning)out of db index range[0 - %d]", s.DbNum-1)))
			// return
		} else {
			if dbIx < 0 {
				// 2.2.1.未超出但低于边界0，发送警告，不处理的话s就给崩了┭┮﹏┭┮
				addReplyBuf(c, CreateObject(ObjTypeString, fmt.Sprintf("(Warning!!!)index out of range [%d]", dbIx)))
			} else {
				// 2.2.2.未超且不低于边界0，切换指向的DB，回送客户端切换成功
				c.Db = s.Db[dbIx]
				addReplyBuf(c, CreateObject(ObjTypeString, "OK"))
			}
		}
	}
}
