package cache

// GlobalExpireTime 全局缓存有效期 单位:秒(s)
const (
	NonExpireTime = -1 // 无过期时间
	// GlobalExpireTime = 18
	GlobalExpireTime = 60 * 30 // 30min
)
