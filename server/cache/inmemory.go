package cache

import (
	"fmt"
	"log"
	"net"
	"os"
	"regexp"
	"strings"
	"sync"
	"time"
)

/*
server结构体
	需要连接的client、命令集
	client
		需要参数、自己的db，命令
	db
		需要数据键值对、过期时间键值对
	命令集
		名称，对应的执行方法
*/
// dict 通用字典
// type dict map[string]interface{}
type dict map[string]*SpecObject

// RocDb 数据库结构体
type RocDb struct {
	Dict    dict // 数据map
	Expires dict // 过期时间map
	mutex   sync.RWMutex
}

// RocCommand 命令集合（Get，Set，Del）（预加：expire，ttl，keys（已完成），select）
type RocCommand struct {
	Name string                     // 命令名称
	Proc func(c *Client, s *Server) // 对应的方法处理process
}

// Server 实例结构体
type Server struct {
	Db       []*RocDb               // 持有的所有DB
	DbNum    int                    // DB数量
	Commands map[string]*RocCommand // 程序所持有的全部命令集（当前有：set、get、del、expire、ttl、keys、select）
}

// Client 与服务端建立连接之后创建一个Client
type Client struct {
	Cmd      *RocCommand   // 当前指令
	Argv     []*SpecObject // 参数们
	Argc     int           // 参数个数
	Db       *RocDb        // 操作的DB，根据select指令可切换
	QueryBuf string        // 客户端给服务器的，一整条
	ReplyBuf string        // 回复给客户端的
}

// 作通知用 管道
// var NotifyCh = make(chan struct{})
// type inMemoryCache struct {
// 对于缓存过期，刚存入用time.Now()设置开始时间，
// 之后计划开启一个go程去轮询缓存中的键值对，使用time.Since()找出时间满足过期条件的进行删除
// 一：一直询问。
// 二：睡一段醒一段询问。
// 三：每次进行更新操作时重新设置过期时间？
// map放的键值，键放里，值和过期时间呢？
// map的值不为[]byte，换为一个其它的结构体，存放值和过期时间？
// dict  map[string][]byte
// 	dict    dict // 键值对
// 	Expires dict // 过期时间
// 	mutex   sync.Mutex
// }

// 1.CreateClient 连接建立之后，创建client记录当前连接
func (s *Server) CreateClient(tcpConn *net.TCPConn) (c *Client) {
	c = new(Client)
	c.Db = s.Db[0]
	c.Argv = make([]*SpecObject, 5) // 也就最多一个命令加两参数(空格也算占位了)
	c.QueryBuf = ""
	return c
}

// 2.ReadQueryInfoFromClient 读取客户端请求信息
func (c *Client) ReadQueryInfoFromClient(tcpConn *net.TCPConn) (err error) {
	buff := make([]byte, 1024)
	n, err := tcpConn.Read(buff)
	if err != nil {
		log.Println("===>>>tcpConn.Read err:", err, "--readLen:", n)
		tcpConn.Close()
		return err
	}
	// 将换行符(win：\r\n)去掉之后拿到整条命令
	// c.QueryBuf = strings.Split(string(buff), "\r\n")[0]
	c.QueryBuf = strings.Split(string(buff), "\n")[0]
	// log.Println("readQueryInfoFromClient's c.QueryBuf:", []byte(c.QueryBuf))
	return nil
}

// 3.ProcessQueryBuf 处理客户端请求信息
func (c *Client) ProcessQueryBuf() {
	// 将空白符去掉，拿指令、键、值
	re := regexp.MustCompile(`[^\s]+`)
	parts := re.FindAllString(strings.Trim(c.QueryBuf, " "), -1)
	argv, argc := parts, len(parts)
	c.Argc = argc // 存放参数个数，正常按本程序为2(get|del|ttl|keys|select)~3(set|expire)个
	for i, v := range argv {
		c.Argv[i] = CreateObject(ObjTypeString, v)
	}
	// log.Println("ProcessQueryBuf's c.argv[2]'s bytes: ", []byte(c.Argv[1].Ptr.(string)))
}

// 4.ProcessCommand 执行命令
func (s *Server) ProcessCommand(c *Client) {
	// 查看指令是否正确
	name, ok := c.Argv[0].Ptr.(string)
	if !ok {
		fmt.Println("===>>>error command")
		os.Exit(1)
	}
	// 4.1.再查看指令是否存在(set|get|del)
	command := findCommand(s, name)
	if command != nil {
		c.Cmd = command
		// 4.2.存在，调用真正的执行操作
		c.Cmd.Proc(c, s)
	} else {
		// 4.3.不存在,说一下就byebye
		addReplyBuf(c, CreateObject(ObjTypeString, fmt.Sprintf("Error: unkonwn command %s", name)))
	}
}

// 4.1.findCommand 查找指令，看是否存在
func findCommand(s *Server, name string) *RocCommand {
	// 到server的指令集去找
	if command, ok := s.Commands[name]; ok {
		return command
	}
	return nil
}

// 4.2.commands找commands.go文件

// 4.3.addReplyBuf 设置回复消息
func addReplyBuf(c *Client, o *SpecObject) {
	if replyStr, ok := o.Ptr.(string); ok {
		c.ReplyBuf = replyStr
	}

}

// 4.3.1.NotifyServerClosed 设置server关闭通知
// func (s *Server) NotifyServerClosed(c *Client, closeNotify string) {
// func (s *Server) NotifyServerClosed(notifyCh chan struct{}) {
// 	// 给server加个clients切片，然后遍历通知？
// 	// 通过一个管道的关闭来通知？
// 	// c.ReplyBuf =
// 	// 关闭通道
// 	close(notifyCh)
// 	log.Println(<-notifyCh)
// }

// 4.4.findKey 查找key
func findKey(cdb *RocDb, key string) *SpecObject {
	if obj, ok := cdb.Dict[key]; ok {
		fmt.Println("===>>>findKey: ", cdb.Dict[key])
		return obj
	}
	fmt.Println("===>>>findKeyFail: ", cdb.Dict[key])
	return nil
}

// 4.5checkExpire 检查缓存是否过期
func expired(cdb *RocDb, key string) bool {
	if startTime, ok := cdb.Expires[key]; ok { // duration ns-----s ms us ns
		// if time.Since(startTime.Ptr.(time.Time))/time.Second < GlobalExpireTime { // 暂定18s过期时间，后期设为常量(解决)，然后加个自定义字段在client中，之后先查有没有自定义再查全局
		if time.Since(startTime.Ptr.(time.Time))/time.Second < 0 { // 暂定18s过期时间，后期设为常量(解决)，然后加个自定义字段在client中，之后先查有没有自定义再查全局(没必要了)
			fmt.Println("===>>>flow time in if: ", time.Since(startTime.Ptr.(time.Time))/time.Second)
			return false
		} else {
			fmt.Println("===>>>flow time in else: ", time.Since(startTime.Ptr.(time.Time))/time.Second)
			return true
		}
	}
	// 没找到，就算过期了
	return true
}
