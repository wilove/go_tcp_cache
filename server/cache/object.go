package cache

// SpecObject 对特定类型进行数据包装，后续进行数据返回或者创建字典用，方便
type SpecObject struct {
	ObjType int
	Ptr     interface{}
}

const ObjTypeString = 1

// CreateObject 创建特定类型的Obj结构
func CreateObject(typ int, ptr interface{}) (o *SpecObject) {
	o = new(SpecObject)
	o.ObjType = typ
	o.Ptr = ptr
	return
}
