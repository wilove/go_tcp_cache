package cache

// unused
type Cache interface {
	Get(string) ([]byte, error)
	Set(string, []byte) error // create|update
	Del(string) error         // delete
}
