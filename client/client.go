package main

import (
	"bufio"
	"fmt"
	"log"
	"net"
	"os"
	"strconv"
	"strings"
)

func main() {
	// 命令行参数处理，暂不考虑
	argv := os.Args
	argc := len(argv)
	if argc > 2 {
		log.Println("Now support arg '-h' or '--help' only, for example you can use: client.exe -h ,or only client.exe to run")
		return
	} else if argc == 2 {
		if argv[1] == "--help" || argv[1] == "-h" {
			cmdUsage()
		} else {
			log.Println("Sorry, now support arg '-h' or '--help' only")
		}
		return
	}
	// 链接处理
	remoteIPPort := "127.0.0.1:9999"
	tmpRemotePrint := remoteIPPort
	tcpAddr, err := net.ResolveTCPAddr("tcp", remoteIPPort)
	// var wg sync.WaitGroup
	checkError(err, nil)
	// nil，自动生成一个本地地址
	tcpConn, err := net.DialTCP("tcp", nil, tcpAddr)
	checkError(err, tcpConn)
	fmt.Println("Hi~")
	reader := bufio.NewReader(os.Stdin)
	// 记得关闭链接(正常主动退出的情况)
	defer func() {
		tcpConn.Close()
		fmt.Println("Connection is closed in defer...")
	}()
	// var dbIx int = 0 // Server DB index
	// 检查server关了没有
	// wg.Add(1)
	// go func(conn net.Conn) {
	// 	for {
	// 		if isServerClosed() {
	// 			fmt.Println("Server is closed...")
	// 			wg.Done()
	// 			break
	// 		} else {
	// 			log.Println("信号呢？？？")
	// 		}
	// 	}
	// 	tcpConn.Close()
	// 	fmt.Println("Connection is closed...")
	// 	os.Exit(1)
	// }(tcpConn)
	for {
		fmt.Print(tmpRemotePrint + "> ")
		inTxt, _ := reader.ReadString('\n')
		// log.Println([]rune(inTxt))
		// 清除回车换行(windows切记\r\n,不然比较不等) \r<=>13 \n<=>10
		// inTxt = strings.Replace(inTxt, "\r\n", "", -1)
		// fmt.Println("compare:", strings.Compare(inTxt, "quit"))
		switch inTxt {
		case "quit\r\n", "exit\r\n":
			fmt.Println("see you next time~")
			return
		default:
			send2Server(tcpConn, inTxt)
		}
		// 接收服务端的响应
		buff := make([]byte, 1024)
		n, err := tcpConn.Read(buff)
		checkError(err, tcpConn)
		if n == 0 {
			fmt.Println(tmpRemotePrint+">", "nil(read's n==0)")
		} else {
			// 打印响应结果
			fmt.Println(string(buff))
			// fmt.Println(remoteIPPort+">", string(buff))
			// 判断是否切换了DB从而对显示进行修改
			if strings.HasPrefix(inTxt, "select ") {
				selectSlice := strings.Split(inTxt, " ")
				// fmt.Println(strings.Compare("OK", strings.ReplaceAll(string(buff), string([]byte{0x00}), "")))
				// fmt.Println([]byte(strings.Replace(string(buff), "\r\n", "", -1)))
				// 1.大于 0 且 返回了OK响应，切换DB
				// 2.大于 0 但 返回不成功，不切换，保持当前
				// 3.等于 0 切回来
				if newDbIx, err := strconv.Atoi(strings.Replace(selectSlice[1], "\r\n", "", -1)); newDbIx >= 0 && (strings.ReplaceAll(string(buff), string([]byte{0x00}), "") == "OK") {
					checkError(err, tcpConn)
					if newDbIx > 0 {
						// dbIx = newDbIx
						tmpRemotePrint = remoteIPPort + "[" + strings.Replace(selectSlice[1], "\r\n", "", -1) + "]"
					} else {
						// dbIx = newDbIx
						tmpRemotePrint = remoteIPPort
					}
				}
				//  else if newDbIx == 0 {
				// 	checkError(err)
				// }
				// else if newDbIx < 0 ||  (strings.ReplaceAll(string(buff), string([]byte{0x00}), "") != "OK")
			}
		}
	}
}

// 将信息写到server
func send2Server(conn net.Conn, inTxt string) (n int, err error) {
	n, err = conn.Write([]byte(inTxt))
	return
}

func checkError(err error, tcpConn *net.TCPConn) {
	if err != nil {
		// if err == io.EOF {
		// 	log.Println("Server is closed...")
		// 	os.Exit(1)
		// } else {
		// 	tcpConn.Close()
		// 	fmt.Println("Connection is closed because err...")
		// 	log.Println("Error:", err.Error())
		// 	os.Exit(1)
		// }
		if tcpConn != nil {
			tcpConn.Close()
			fmt.Println("Connection is closed...")
		}
		log.Println("Error:", err.Error())
		os.Exit(1)
	}
}

// 命令行参数处理
func cmdUsage() {
	fmt.Println("command usage of go_tcp_cache")
	fmt.Println("commands:set|get|del|expire|ttl|keys|select")
	fmt.Println("Usage:")
	fmt.Println("\tset\tset key value\teg: set keyStr valStr\t\"set keyStr-valStr to map\"")
	fmt.Println("\tget\tget key\teg: get keyStr\t\"get val of key in map\"")
	fmt.Println("\tdel\tdel key\teg: del keyStr\t\"del k-v from map by k\"")
	fmt.Println("\texpire\texpire key time_of_seconds\teg: expire keyStr 120\t\"k-v in map will expire after 120 seconds\"")
	fmt.Println("\tttl\tttl key\teg: ttl keyStr\t\"lookup the time to live of keyStr\"")
	fmt.Println("\tkeys\tkeys words_or_regexp\teg: keys keyStr | keys *St* | keys key* | keys *Str\t\"find keys in current map\"")
	fmt.Println("\tselect\tselect db_ix\teg: select 1\t\"select which db want to op\"")
}

// 检查server是否关闭
// func isServerClosed() bool {
// 	select {
// 	case <-cache.NotifyCh:
// 		return true
// 	default:
// 	}
// 	return false
// }
