启动步骤：
先运行server.exe，在运行client.exe

可使用命令

1、set命令
使用示例：

set key value >> 设置键为key，值为value的键值对

2、get命令
使用示例：

get key >> 获取键为key的值value

3、del命令
使用示例：

del key >> 删除键为key的键值对

4、expire命令
使用示例：

expire key time >> 设置键为key的过期时间，time 以秒(s)为单位，设置为0及以下表示现在就过期

5、ttl命令
使用示例：

ttl key >> 查询键为key的剩余可存活时间

6、keys命令
使用示例：

keys * >> 查询所有的键

keys key* >> 查询以"key"开头的键

keys *key >> 查询以"key"结尾的键

keys *key* >> 查询以"key"为中间内容的键

keys key1*key2 >> 查询以"key1"开头，"key2"结尾的键
......
暂不支持其他匹配符号

7、select命令
使用示例：

select num >> 切换访问的服务器DB，num为数字，但不可为负数，也不可超过服务器所持有的DB数
